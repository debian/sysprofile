# See either "man alias" or "man bash_builtins" for
# further explanation of the shell's alias mechanism.

# Some random aliases:

alias lo='clear;logout'
alias startx='startx 2> ~/.X.err 1> ~/.X.out &'
alias xwin='startx; logout'
alias ll='ls -l'

# Some things don't function properly if TERM=emacs

if [ `echo $TERM` != emacs ] && [ `echo $TERM` != dumb ]; then
    alias man='man -P less'
    alias ls='ls --color=auto '
    alias v='vdir --color=auto'
else
    alias v='/bin/ls -l'
    alias man='echo -e "Please use \"M-x manual-entry RET manpage\"\nto read the manual page for"'
fi

# A few abbreviations for using recode

if [ `which recode` ]; then
    alias atari2unix='recode AtariST..latin1'       
    alias unix2dos='recode lat1..ibmpc'
    alias dos2unix='recode ibmpc..lat1'
fi

# Sourcing all private aliases:

if [ -f ~/.alias ]; then
    . ~/.alias
fi
