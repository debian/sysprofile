# Define here the most appropriate editor for all.
# Any complaints about the order of arguments go
# immediately to "/dev/null". If you don't like 
# the ordering then just change it.

#set -x

if [ -x /usr/bin/mcedit ]; then
    export EDITOR=mcedit
elif [ -x /usr/bin/vi ]; then
    export EDITOR=vi
elif [ -x /usr/bin/elvis-tiny ]; then
    export EDITOR=elvis-tiny
elif [ -x /usr/bin/jed ]; then
    export EDITOR=jed
elif [ -x /usr/bin/emacs ]; then
    export EDITOR=emacs
elif [ -x /usr/bin/xemacs ]; then
    export EDITOR=xemacs
fi


