# With this configuration setting the GNU Midnight Commander leaves you
# to stay right at the same spot you were last upon quit.

#set -x

# If you prefer MC in monochrome
# then just uncomment this:

#unset COLORTERM
#unset COLORFGBG

if [ `echo $TERM` != emacs ] && [ `echo $TERM` != dumb ]; then

# This works only with mc-4.6.0 and above:
mc ()
{
MC_PWD_FILE="${TMPDIR-/tmp}/mc-$USER/mc.pwd.$$"
/usr/bin/mc -bP "$MC_PWD_FILE" "$@"

if test -r "$MC_PWD_FILE"; then
	MC_PWD="`cat $MC_PWD_FILE`"
	if test -n "$MC_PWD" && test -d "$MC_PWD"; then
		cd "$MC_PWD"
	fi
	unset MC_PWD
fi

rm -f "$MC_PWD_FILE"
unset MC_PWD_FILE
}

# This works only for mc-4.5.x or lower:
#
#mc ()
#{
#    mkdir -p ~/.mc/tmp 2> /dev/null
#	chmod 700 ~/.mc/tmp
#	MC=~/.mc/tmp/mc-$$
#	/usr/bin/mc -P "$@" > "$MC"
#	cd "`cat $MC`"
#	rm "$MC"
#    unset MC;
#}

fi
