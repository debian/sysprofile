# This is for setting GNUstep specific environment variables

#set -x

if [ ! $GNUSTEP_ROOT ] && [ -f /usr/lib/GNUstep/System/Makefiles/GNUstep.sh ]; then
    . /usr/lib/GNUstep/System/Makefiles/GNUstep.sh &>/dev/null
fi
