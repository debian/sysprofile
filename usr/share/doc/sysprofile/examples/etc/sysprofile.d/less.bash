# See "man less" for details of further customization

#set -x

# assures the correct displaying of 8-bit 
# characters in the latin1 charset.  

export LESSCHARSET=latin1

# This option regulates the verbosity of "less".

export LESS=-MM

# enables browsing of .deb, .rpm,. tar.gz, etc. with less only

eval `/usr/bin/lesspipe`
