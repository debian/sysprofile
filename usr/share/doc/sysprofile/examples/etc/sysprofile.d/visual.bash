# Define here the most appropriate VISUAL for all.
# Any complaints about the order of arguments go
# immediately to "/dev/null". If you don't like 
# the ordering then just change it.

#set -x

if [ -x /usr/bin/mcedit ]; then
    export VISUAL=mcedit
elif [ -x /usr/bin/vi ]; then
    export VISUAL=vi
elif [ -x /usr/bin/elvis-tiny ]; then
    export VISUAL=elvis-tiny
elif [ -x /usr/bin/jed ]; then
    export VISUAL=jed
elif [ -x /usr/bin/emacs ]; then
    export VISUAL=emacs
elif [ -x /usr/bin/xemacs ]; then
    export VISUAL=xemacs
fi


