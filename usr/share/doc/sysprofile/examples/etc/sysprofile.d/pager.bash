# The pager is used for viewing files and is used for displaying manual
# pages as well.  The following settings defines the use of "less".

#set -x

export PAGER=less
