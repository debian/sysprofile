# Defining a compose key for the console:

if [ ! TERM = xterm-debian ]; then
loadkeys <<EOF
shift keycode 100 = Compose
altgr keycode 42 = Compose
EOF
fi &>/dev/null
