# Define here the most appropriate editor for all.
# Any complaints about the order of arguments go
# immediately to "/dev/null". If you don't like 
# the ordering then just change it.

if [ -x $HOME/bin/editclient ]; then
    export EDITOR=editclient
else
    . /etc/sysprofile.d/editor.bash
fi


