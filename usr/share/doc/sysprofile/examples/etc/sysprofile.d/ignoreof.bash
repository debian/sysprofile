# This prevents a logout with just a single ctrl-d:

#set -x

export IGNOREEOF=1
