# System-wide .bashrc file for interactive bash(1) shells.

shopt -s checkwinsize

if [ -f /etc/sysprofile ]; then
  if [ `grep -x ^SYSDEBUG=1 /etc/sysprofile` ]; then
    if [ ! -d /var/tmp/sysprofile ]; then
        mkdir -m 1777 /var/tmp/sysprofile
        touch /var/tmp/sysprofile/$USER
        chmod 600 /var/tmp/sysprofile/$USER
    fi
    . /etc/sysprofile >> /var/tmp/sysprofile/$USER 2>&1
  else
    . /etc/sysprofile
  fi
fi
                                   