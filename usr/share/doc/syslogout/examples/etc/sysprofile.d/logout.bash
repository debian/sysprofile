# For using "syslogout" with X11, better invoke it from out of your
# X display manager's Xreset file as explained in README.usage instead
# out of "$HOME/.bash_logout".
#
# For using "syslogout" exclusively in a non-X11 environment you could
# use this code here via the "syslogin" package to make sure every user 
# has a "$HOME/.bash_logout" file:

if [ ! -f $HOME/.bash_logout ]
  then
    if [ -f /etc/skel/.bash_logout ]
      then
        cp /etc/skel/.bash_logout $HOME
    fi
fi
