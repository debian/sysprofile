# Prevents sysprofile logfiles from consuming too much space

# Uncomment for debugging:
#set -x

if [ -f /var/tmp/sysprofile/$USER ]; then
     rm -f /var/tmp/sysprofile/$USER
fi
