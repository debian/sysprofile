syslogout for Debian
----------------------

The package "syslogout" is a modular centralized logout configuration
for the bash shell within a Debian Linux system.  It's main component is
the directory "/etc/syslogout.d/" which contains small shell script
modules executed at logout of each user.

The contents of "/etc/syslogout.d/" serve to establish a system wide
mechanism to execute e.g. clean up routines after each user's logout.

While some assorted sample shell script modules are provided as
examples, the main accent lies on the implementation of a flexible
modular mechanism instead of a specific but limited configuration
setting which doesn't match anything but its author's own systems.

The syslogout mechanism appears to be quite usable and has actually
been used successfully for more than a year. It still lacks further
thought and polishing by peer review, though.  Just take this here as a
first sample implementation of an idea for now.  Please examine and test
it closely before any serious use and share any criticism, corrections
and hopefully additions with all of us.  Especially nice would be the
addition of somewhat equivalent settings for other shells like (t)csh,
ksh or zsh, since this package primarily works for bash and has not been
tested otherwise.

If you need a similar mechanism for executing code at login time please
check out the related package "sysprofile" by yours truly, which is a
very close companion to "syslogout".

Debian package maintainers please note:
---------------------------------------

Never *ever* rely on the existence of syslogout on a system for your
own Debian packages.  The syslogout mechanism is and should stay an
optional tool for local sysadmins only.

Massimo Dal Zotto <dz@cs.unitn.it> has developed a similar package
called "shellrc" which provides functionality not only for bash.  It
is hopefully still available from "http://www.cs.unitn.it/~dz/debian/".

If you happen to become inspired enough to make something really useful
out of this package i would be glad to hand over maintenance.  I just
tried to hack something i could use for my own purposes and because i
didn't find anything equally simple to use.

Paul Seelig <pseelig@debian.org>, Mon, 01 Oct 2001 21:13:54 +0200
